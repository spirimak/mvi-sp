Conditional image generation with a diffusion model

Latent diffusion models expand on the concept of diffusion models. These are probabilistic models capable of crafting top-notch images: they begin with random noise and, through a gradual diffusion process, transform it into realistic, high-quality images.
The goal of the project is to demonstrate their usefulness in generating images that correspond directly to provided category of images. Images of landscapes will be generated after training on a [Intel Image Classification dataset](https://www.kaggle.com/datasets/puneet6060/intel-image-classification).

The report can be accessed here: [report](https://gitlab.fit.cvut.cz/spirimak/mvi-sp/-/blob/master/report.pdf).
